open Cx

@module external styles: {..} = "./style.module.styl"
@module external toggleSrc: {..} = "./toggle.svg"

type onToggle = unit => unit

@react.component
let make = (~onToggle, ~isOpen: bool, ~choice: option<string>) => {
  <div onClick={_ => onToggle()} role="button" className={styles["button"]}>
    <div className={styles["content"]}>
      <span> {choice->Belt.Option.getWithDefault("Select country") |> React.string} </span>
      <div className={cx([styles["icon"], isOpen ? styles["rotated"] : ""])}>
        <img src={toggleSrc["default"]} />
      </div>
    </div>
  </div>
}
