type option_<'v> = {
  label: string,
  value: 'v,
}

type options<'v> = array<option_<'v>>

type components<'v> = {
  @as("Control") control: React.component<{"children": React.element}>,
  @as("IndicatorsContainer") indicatorsContainer: React.component<{.}>,
  @as("Option")
  option: React.component<{
    "children": React.element,
    "innerProps": {.},
    "value": 'v,
  }>,
}

module Select = {
  @react.component @module("react-select")
  external make: (
    ~options: options<'v>,
    ~value: Js.Nullable.t<string>=?,
    ~backspaceRemovesValue: bool=?,
    ~controlShouldRenderValue: bool=?,
    ~hideSelectedOptions: bool=?,
    ~isClearable: bool=?,
    ~menuIsOpen: bool=?,
    ~tabSelectsValue: bool=?,
    ~autoFocus: bool=?,
    ~onMenuClose: unit => unit=?,
    ~onBlur: {..} => unit=?,
    ~components: components<'v>=?,
    ~styles: {"valueContainer": {..} => {..}, "menu": {..} => {..}, "control": {..} => {..}}=?,
    ~onChange: option_<'v> => unit,
  ) => React.element = "default"
}

@module("react-select") external components: components<'v> = "components"
