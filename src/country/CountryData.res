type countries = ReactSelect.options<string> // Business logic leaks slightly due to access to library (ReactSelect) types, but they structural identity, why not

@scope("JSON") @val
external parse: string => countries = "parse"

type response

@val
external fetch: string => Promise.t<response> = "fetch"

@send
external text: response => Promise.t<string> = "text"

let fetchCountries = () => {
  // Unfortunately bs-let doesn't work with rescript
  // https://github.com/reasonml-labs/bs-let/issues/30
  // let%Promise result = fetch("https://gist.githubusercontent.com/rusty-key/659db3f4566df459bd59c8a53dc9f71f/raw/4127f9550ef063121c564025f6d27dceeb279623/counties.json")
  fetch(
    "https://gist.githubusercontent.com/rusty-key/659db3f4566df459bd59c8a53dc9f71f/raw/4127f9550ef063121c564025f6d27dceeb279623/counties.json",
  )
  ->Promise.flatMap(text)
  ->Promise.map(parse)
}
