open ReactSelect
open Cx

@module external lensSrc: {..} = "./lens.svg"
@module external styles: {..} = "./styles.module.styl"

type countries = ReactSelect.options<string>

@lenses
type state = {
  isOpen: bool,
  options: countries,
}
type action = CountriesLoaded(countries) | UpdateOpen | Close

let initialState = {
  isOpen: false,
  options: [],
}

let reducer = (state, action) =>
  switch action {
  | CountriesLoaded(c) => state->state_set(Options, c)
  | UpdateOpen => state->state_set(IsOpen, !state.isOpen)
  | Close => state->state_set(IsOpen, false)
  }

module Control = {
  let make = (props: {..}) => {
    let withChildren =
      <>
        <div style={ReactDOM.Style.make(~marginLeft="10px", ())}>
          <img src={lensSrc["default"]} />
        </div>
        {props["children"]}
      </>
    let p = Js.Obj.assign(
      Js.Obj.assign(Js.Obj.empty(), props),
      Js.Obj.assign(Js.Obj.empty(), {"children": withChildren}),
    )
    <div> {React.createElement(components.control, p)} </div>
  }
}

module IndicatorsContainer = {
  let make = _ => React.null
}

module Option = {
  let make = props => {
    let withChildren =
      <div className={styles["option"]}>
        <div> <span className={cx(["fi", `fi-${props["value"]}`])} /> </div>
        <div className={styles["content"]}> {props["children"]} </div>
      </div>
    let p = Js.Obj.assign(
      Js.Obj.assign(Js.Obj.empty(), props),
      Js.Obj.assign(Js.Obj.empty(), {"children": withChildren}),
    )
    <div> {React.createElement(components.option, p)} </div>
  }
}

let reactSelectStyles = {
  "valueContainer": p =>
    Js.Obj.assign(
      Js.Obj.assign(Js.Obj.empty(), p),
      Js.Obj.assign(Js.Obj.empty(), {"padding": "8px 16px 8px 8px", "fontFamily": "Arial"}),
    ),
  "menu": p =>
    Js.Obj.assign(
      Js.Obj.assign(Js.Obj.empty(), p),
      Js.Obj.assign(Js.Obj.empty(), {"marginTop": "0"}),
    ),
  "control": p =>
    Js.Obj.assign(
      Js.Obj.assign(Js.Obj.empty(), p),
      Js.Obj.assign(
        Js.Obj.empty(),
        {
          "border": "1px solid rgba(0,0,0,0.20)",
          "boxShadow": "none",
          "&:hover": {
            "border": "1px solid rgba(0,0,0,0.20)",
          },
        },
      ),
    ),
}

@react.component
let make = (~value: option<string>, ~onChange: option_<string> => unit) => {
  let (state, dispatch) = React.useReducer(reducer, initialState)
  // let%Option v = value;
  // let%Option element = Js.Array.find(i => i.value == v, state.options);
  // Unfortunately bs-let doesn't work with rescript
  // https://github.com/reasonml-labs/bs-let/issues/30
  let choice =
    value
    ->Belt.Option.flatMap(v => Js.Array.find(i => i.value == v, state.options))
    ->Belt.Option.map(v => v.label)
  React.useEffect0(() => {
    open CountryData
    fetchCountries()->Promise.get(r => {
      dispatch(CountriesLoaded(r))
    })
    None
  })
  <div>
    <ToggleButton choice isOpen=state.isOpen onToggle={() => dispatch(UpdateOpen)} />
    {state.isOpen
      ? <div className={styles["selectWrap"]}>
          <Select
            options={state->state_get(Options)}
            value={Js.Nullable.fromOption(value)}
            backspaceRemovesValue=false
            controlShouldRenderValue=false
            hideSelectedOptions=false
            isClearable=false
            menuIsOpen=true
            autoFocus=true
            tabSelectsValue=false
            onMenuClose={() => dispatch(Close)}
            components={
              control: Control.make,
              indicatorsContainer: IndicatorsContainer.make,
              option: Option.make,
            }
            styles=reactSelectStyles
            onChange
          />
        </div>
      : React.null}
  </div>
}
