@module external styles: {..} = "./style.module.styl"

@react.component
let make = () => {
  let (state, setState) = React.useState(_ => None)
  <div className={styles["content"]}>
    <CountrySelect
      value=state
      onChange={c => {
        setState(_ => Some(c.value))
      }}
    />
  </div>
}
